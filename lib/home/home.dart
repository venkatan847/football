import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/match_provider.dart';

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MatchProvider provider = Provider.of<MatchProvider>(context);
    final matchData = provider.matchResultsData;
    double size = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(
          title: Text(provider.matchesData?.competition.name ?? ''),
          centerTitle: true,
        ),
        body: provider.isLoading
            ? const Center(child: CircularProgressIndicator())
            : Column(children: [
                const SizedBox(
                  height: 30,
                ),
                Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    width: size,
                    child: Text(
                      "Highest Matches Winning Team: ${provider.highestWinningTeam ?? ''}",
                      style: const TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 16,
                          color: Colors.teal),
                    )),
                Container(
                  width: 200,
                  margin: const EdgeInsets.symmetric(vertical: 20),
                  child: TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, "/team");
                    },
                    child: Text(
                      '${provider.highestWinningTeam ?? ''} Info',
                      style: const TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  color: Colors.teal,
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                    padding: const EdgeInsets.only(right: 25),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        labelData('Team 1'),
                        labelData('Team 2'),
                        labelData('Winner'),
                      ],
                    )),
                Expanded(
                    child: ListView.builder(
                        physics: const AlwaysScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: provider.matchResultsData.length,
                        itemBuilder: (context, index) {
                          dynamic matchData = provider.matchResultsData[index];
                          return Padding(
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  textData(matchData.homeTeamName, size),
                                  textData(matchData.awayTeamName, size),
                                  textData(matchData.winner[index], size),
                                ]),
                            padding: const EdgeInsets.symmetric(horizontal: 0),
                          );
                        }))
              ]));
  }

  Widget textData(String? textData, double size) {
    return Container(
        alignment: Alignment.centerLeft,
        height: 40,
        width: size / 3,
        child: Text(textData ?? ''));
  }

  Widget labelData(String? textData) {
    return Text(
      textData ?? '',
      style: const TextStyle(
          fontWeight: FontWeight.w700, fontSize: 16, color: Colors.blue),
    );
  }
}

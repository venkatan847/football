import 'package:flutter/material.dart';
import '../team_info/team_info_page.dart';
import 'home.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(appBarTheme: const AppBarTheme(
          color: Colors.blue,
          iconTheme: IconThemeData(color: Colors.white)
      ),
          cardTheme: const CardTheme(color: Colors.white)),
      initialRoute: "/home",
      routes:{ "/home" : (BuildContext context) => const MyHomePage(),
        "/team" : (BuildContext context) => const TeamInfo(),
      },
      home: const MyHomePage(),
    );
  }
}

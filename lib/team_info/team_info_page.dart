import 'package:bt_task/team_info/player_info.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/match_provider.dart';
import '../providers/team_provider.dart';
import 'address_info.dart';

class TeamInfo extends StatelessWidget {
  const TeamInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MatchProvider mProvider = Provider.of<MatchProvider>(context);
    TeamProvider provider = Provider.of<TeamProvider>(context);
    provider.getData(mProvider.teamId);
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text('${provider.teamData?.name ?? ''} Info'),
          centerTitle: true,
        ),
        body: provider.isLoading
            ? const Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                child: Column(
                children: [
                  Container(
                      width: MediaQuery.of(context).size.width - 10,
                      padding:
                          const EdgeInsets.only(left: 5, top: 10, right: 5),
                      child: Card(
                          child: Column(
                        children: [
                          const Center(
                            child: Text(
                              'About Team',
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.red,
                                  fontWeight: FontWeight.w700),
                            ),
                          ),
                          textData(
                              'Area :', provider.teamData?.area.name ?? ''),
                          textData('Name :', provider.teamData?.name ?? ''),
                          textData('Short Name :',
                              provider.teamData?.shortName ?? ''),
                          textData(
                              'Founded :', '${provider.teamData?.founded}'),
                          textData('Venue :', provider.teamData?.venue ?? ''),
                          Container(
                              alignment: Alignment.center,
                              height: 150,
                              width: 150,
                              child: Image.network(
                                provider.teamData?.crestUrl ?? '',
                                fit: BoxFit.cover,
                              ))
                        ],
                      ))),
                  const SizedBox(
                    height: 15,
                  ),
                  PlayerInfo(teamData: provider.teamData),
                  const SizedBox(
                    height: 15,
                  ),
                  AddressInfo(teamData: provider.teamData)
                ],
              )));
  }

  Widget textData(String name, String value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
            alignment: Alignment.center,
            width: 150,
            height: 40,
            child: Text(
              name,
              style: const TextStyle(fontSize: 15),
            )),
        Container(
          alignment: Alignment.centerLeft,
          padding: const EdgeInsets.only(left: 20),
          width: 150,
          height: 40,
          child: Text(value, style: const TextStyle(fontSize: 15)),
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';

class PlayerInfo extends StatelessWidget {
  PlayerInfo({Key? key, required this.teamData}) : super(key: key);
  var teamData;

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      const Center(
        child: Text(
          'Team Players',
          style: TextStyle(
              fontSize: 18, color: Colors.red, fontWeight: FontWeight.w700),
        ),
      ),
      ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: teamData.squad?.length,
          itemBuilder: (context, index) {
            final data = teamData.squad[index];
            return Card(
                child: Container(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    height: 250,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Center(
                            child: Text(
                          data.name,
                          style:
                              const TextStyle(color: Colors.teal, fontSize: 18),
                        )),
                        infoData('Role :', data.role),
                        infoData('Position :', data.position),
                        infoData('Nationality :', data.nationality),
                        infoData('Data Of Birth :',
                            data.dateOfBirth.substring(0, 10)),
                        infoData('Country Of Birth :', data.countryOfBirth),
                      ],
                    )));
          })
    ]);
  }

  Widget infoData(String name, String value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(padding:const EdgeInsets.only(left: 20),
            alignment:  Alignment.centerLeft,
            width: 150,
            height: 40,
            child: Text(
              name,
              style: const TextStyle(fontSize: 15),
            )),
        Container(
          alignment: Alignment.centerLeft,
          padding: const EdgeInsets.only(left: 10),
          width: 150,
          height: 40,
          child: Text(value, style: const TextStyle(fontSize: 15)),
        ),
      ],
    );
  }
}


import 'package:flutter/material.dart';

import '../response/teams_response/teams_response.dart';

class AddressInfo extends StatelessWidget {
  var teamData;

   AddressInfo({Key? key, required this.teamData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      const Center(
        child: Text(
          'Address',
          style: TextStyle(
              fontSize: 18, color: Colors.red, fontWeight: FontWeight.w700),
        ),
      ),
      Card(
          child: Container(
        height: 200,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            dataInfo('Address :', teamData.address),
            dataInfo('Phone Number :', teamData.phone),
            dataInfo('Email :', teamData.email),
            dataInfo('Website Link :', teamData.website),
          ],
        ),
      ))
    ]);
  }

  Widget dataInfo(String name, String value) {
    return  Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
            alignment: Alignment.center,
            width: 200,
            height: 40,
            child: Text(
              name,
              style: const TextStyle(fontSize: 15),
            )),
        Container(
          alignment: Alignment.centerLeft,
          width: 200,
          height: 40,
          child: Text(value, style: const TextStyle(fontSize: 15)),
        ),
      ],
    );
  }
}

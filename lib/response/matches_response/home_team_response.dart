class HomeTeam {
  String name;
  int id;

  HomeTeam({required this.name,required this.id});

  factory HomeTeam.fromJson(Map<String, dynamic> json) {
    return HomeTeam(
      name: json['name'],
      id: json['id'],
    );
  }
}
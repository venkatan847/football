
import 'package:bt_task/response/matches_response/score_response.dart';
import 'package:bt_task/response/matches_response/season_response.dart';

import 'away_team_response.dart';
import 'home_team_response.dart';

class Matches {
  int id;
  Season season;
  String utcDate;
  String status;
  int matchday;
  String stage;

  String lastUpdated;
  Score score;
  HomeTeam homeTeam;
  AwayTeam awayTeam;

  Matches(
      {required this.id,
        required this.season,
        required this.utcDate,
        required this.status,
        required this.matchday,
        required this.stage,
        required this.lastUpdated,
        required this.score,
        required this.homeTeam,
        required this.awayTeam});

  factory Matches.fromJson(Map<String, dynamic> json) {
    return Matches(
        id: json['id'],
        season: Season.fromJson(json['season']),
        utcDate: json['utcDate'],
        status: json['status'],
        matchday: json['matchday'],
        stage: json['stage'],
        lastUpdated: json['lastUpdated'],
        score:Score.fromJson(json['score']),
        homeTeam: HomeTeam.fromJson(json['homeTeam']),
        awayTeam: AwayTeam.fromJson(json['awayTeam']));
  }
}
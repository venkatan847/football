class Season {
  int id;
  String startDate;
  String endDate;
  int currentMatchday;

  Season(
      {required this.id,
        required this.startDate,
        required this.endDate,
        required this.currentMatchday});

  factory Season.fromJson(Map<String, dynamic> json) {
    return Season(
      id: json['id'],
      startDate: json['startDate'],
      endDate: json['endDate'],
      currentMatchday: json['currentMatchday'],
    );
  }
}
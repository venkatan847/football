import 'area_response.dart';

class Competition {
  int id;
  Area area;
  String name;
  String code;
  String plan;
  String lastUpdated;
  Competition(
      {required this.id,
        required this.area,
        required this.name,
        required this.code,
        required this.plan,
        required this.lastUpdated});

  factory Competition.fromJson(Map<String, dynamic> json) {
    return Competition(
        id: json['id'],
        area: Area.fromJson(json['area']),
        name: json['name'],
        code: json['code'],
        plan: json['plan'],
        lastUpdated: json['lastUpdated']);
  }
}
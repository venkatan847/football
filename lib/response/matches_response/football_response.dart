import 'competition_response.dart';
import 'matches_response.dart';

class FootBallResponse {
  int count;
  Competition competition;
  List<Matches> matches;

  FootBallResponse(
      {required this.count, required this.competition, required this.matches});

  factory FootBallResponse.fromJson(Map<String, dynamic> json) {
    return FootBallResponse(
        count: json['count'],
        competition:Competition.fromJson(json['competition']),
        matches: List<Matches>.from(
            json['matches'].map((e) => Matches.fromJson(e))));
  }
}













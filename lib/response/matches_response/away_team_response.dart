class AwayTeam {
  String name;
  int id;

  AwayTeam({required this.name,required this.id});

  factory AwayTeam.fromJson(Map<String, dynamic> json) {
    return AwayTeam(
      name: json['name'],
      id : json['id']
    );
  }
}
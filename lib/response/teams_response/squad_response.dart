class Squad {
  int id;
  String name;
  String position;
  String dateOfBirth;
  String countryOfBirth;
  String nationality;
  String role;

  Squad({required this.id,
    required this.name,
    required this.position,
    required this.dateOfBirth,
    required this.countryOfBirth,
    required this.nationality,
    required this.role});

  factory Squad.fromJson(Map<String, dynamic> json) {
    return Squad(
        id : json['id'],
        name : json['name'],
        position : json['position'],
        dateOfBirth : json['dateOfBirth'],
        countryOfBirth : json['countryOfBirth'],
        nationality : json['nationality'],
        role : json['role']);
  }

}
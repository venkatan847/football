import 'package:bt_task/response/teams_response/squad_response.dart';

import '../matches_response/area_response.dart';

class TeamResponse {
  Area area;
  String name;
  String shortName;
  String tla;
  String crestUrl;
  String address;
  String phone;
  String website;
  String email;
  int founded;
  String clubColors;
  String venue;
  List<Squad>? squad;

  TeamResponse({
    required this.area,
    required this.name,
    required this.shortName,
    required this.tla,
    required this.crestUrl,
    required this.address,
    required this.phone,
    required this.website,
    required this.email,
    required this.founded,
    required this.clubColors,
    required this.venue,
    this.squad,
  });

  factory TeamResponse.fromJson(Map<String, dynamic> json) {
    return TeamResponse(
        area: Area.fromJson(json['area']),
        name: json['name'],
        shortName: json['shortName'],
        tla: json['tla'],
        crestUrl: json['crestUrl'],
        address: json['address'],
        phone: json['phone'],
        website: json['website'],
        email: json['email'],
        founded: json['founded'],
        clubColors: json['clubColors'],
        venue: json['venue'],
        squad: List<Squad>.from(json['squad'].map((e) => Squad.fromJson(e))));
  }
}

import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'dart:convert' as convert;
import '../apis/api_util.dart';
import '../response/teams_response/teams_response.dart';

class TeamProvider extends ChangeNotifier {
  bool isLoading = true;
  TeamResponse? teamData;

  void getData(String? teamId) async {
    try {
      var response = await http.get('${APIsUtil.teamsUrl}/$teamId',
          headers: {"X-Auth-Token": "12f9a5c897ef4db492b9ca4c0e641d14"});
      var jsonResponse = convert.jsonDecode(response.body);

      teamData = TeamResponse.fromJson(jsonResponse);
      isLoading = false;
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }
}

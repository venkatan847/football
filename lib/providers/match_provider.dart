import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'dart:convert' as convert;
import '../apis/api_util.dart';
import '../response/matches_response/football_response.dart';

class MatchProvider extends ChangeNotifier {
  var matchesData;
  bool isLoading = true;
  String? teamId;
  dynamic highestWinningTeam;
  List<MatchResults> matchResultsData = [];
  List<String> winner = [];
  int indexValue = 0;

  MatchProvider() {
    getData();
  }

  void getData() async {
    try {
      var response = await http.get(APIsUtil.matchesUrl,
          headers: {"X-Auth-Token": "12f9a5c897ef4db492b9ca4c0e641d14"});
      var jsonResponse = convert.jsonDecode(response.body);
      matchesData = FootBallResponse.fromJson(jsonResponse);
      isLoading = false;
      notifyListeners();
    } catch (e) {
      print(e);
    }
    getMatchInfo();
  }

  getMatchInfo() {
    for (var data in matchesData.matches) {
      String winnerValue = data.score.winner ?? '';
      if (winnerValue != '') {
        DateTime matchDate = DateTime.parse(data.utcDate);
        bool isTrue = DateTime.now()
            .subtract(const Duration(days: 31))
            .isBefore(matchDate);
        if (isTrue) {
          if (data.score.winner == 'AWAY_TEAM') {
            winner.add(data.awayTeam.name);
          } else if (data.score.winner == 'HOME_TEAM') {
            winner.add(data.homeTeam.name);
          } else {
            winner.add(data.score.winner);
          }
          matchResultsData.add(MatchResults(
              "${data.homeTeam.id}",
              "${data.awayTeam.id}",
              data.homeTeam.name,
              data.awayTeam.name,
              winner));
        }
      }
    }
    dynamic list = winner;
    final folded = list.fold({}, (acc, curr) {
      acc[curr] = (acc[curr] ?? 0) + 1;
      return acc;
    }) as Map<dynamic, dynamic>;
    final sortedKeys = folded.keys.toList()
      ..sort((a, b) => folded[b].compareTo(folded[a]));

    if (sortedKeys[0] == 'DRAW') {
      highestWinningTeam = sortedKeys[1];
    } else {
      highestWinningTeam = sortedKeys[0];
    }

    notifyListeners();
    getTeamId();
  }

  void getTeamId() {
    teamId = '';
    for (var data in matchResultsData) {
      if (teamId == '') {
        String awayName = data.awayTeamName ?? '';
        String homeName = data.awayTeamName ?? '';
        if (awayName.contains(highestWinningTeam)) {
          teamId = data.awayTeamId;
        } else if (homeName.contains(highestWinningTeam)) {
          teamId = data.homeTeamId;
        }
      }
    }
    notifyListeners();
  }
}

class MatchResults {
  String? homeTeamId;
  String? awayTeamId;
  String? homeTeamName;
  String? awayTeamName;
  List<String>? winner;

  MatchResults(
    this.homeTeamId,
    this.awayTeamId,
    this.homeTeamName,
    this.awayTeamName,
    this.winner,
  );
}

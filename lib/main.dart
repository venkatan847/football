import 'package:bt_task/providers/match_provider.dart';
import 'package:bt_task/providers/team_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'home/my_app.dart';

void main() {
  runApp(
      MultiProvider(
          providers: [
            ChangeNotifierProvider<MatchProvider>(create: (_) => MatchProvider()),
            ChangeNotifierProvider<TeamProvider>(create: (_) => TeamProvider() ),
          ],
          child: const MyApp()));
}



abstract class APIsUtil {
  static const String baseUrl = 'https://api.football-data.org';
  static const String matchesUrl = '$baseUrl/v2/competitions/PL/matches';
  static const String teamsUrl = '$baseUrl/v2/teams';
}